from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework import status
from rest_framework.response import Response

from shapeapi import serializers, models


class UserViewSet(viewsets.ViewSet):
    """
    User API View Sets
    """
    def list(self, request):
        instance = User.objects.all()
        serializer = serializers.UserSerializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=["POST"], detail=False, permission_classes=[], authentication_classes=[])
    def register(self, request):
        payload = request.data
        serializer = serializers.UserSerializer(data=payload)

        if serializer.is_valid():
            serializer.save()
            return Response({
                'message': "User successfully register!"
            }, status=status.HTTP_200_OK)
        else:
            return Response({
                "message": "Bad request!",
                "details": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=["POST"], detail=False, permission_classes=[], authentication_classes=[])
    def login(self, request):
        payload = request.data
        username = payload.get("username")
        password = payload.get("password")

        user = User.objects.filter(username=username).first()

        if not user or not user.check_password(password):
            return Response({
                'message': "Invalid Username/Password"
            }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            'message': "Login successful!"
        }, status=status.HTTP_200_OK)


class ShapeViewSet(viewsets.ViewSet):

    """ This function will create or update shapes """
    def create_or_update_shape(self, serializer):
        message = "Successfully created!"

        if serializer.instance:
            message = "Successfully updated!"

        if serializer.is_valid():
            serializer.save()
            return Response({
                'message': message
            }, status=status.HTTP_200_OK)
        else:
            return Response({
                "message": "Bad Request!",
                "details": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)


class TriangleViewSet(ShapeViewSet):
    def list(self, request):
        instance = models.TriangleModel.objects.all()
        serializer = serializers.TriangleSerializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        payload = request.data
        serializer = serializers.TriangleSerializer(data=payload)
        return super().create_or_update_shape(serializer)

    def update(self, request, pk=None):
        payload = request.data
        instance = models.TriangleModel.objects.filter(pk=pk).first()
        serializer = serializers.TriangleSerializer(instance=instance, data=payload)
        return super().create_or_update_shape(serializer)

    def retrieve(self, request, pk=None):
        instance = models.TriangleModel.objects.filter(pk=pk).first()
        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = serializers.TriangleSerializer(instance=instance, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        if models.TriangleModel.objects.filter(pk=pk).delete():
            return Response({'message': "Successfully Deleted!"}, status=status.HTTP_200_OK)

        return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def area(self, request, pk=None):
        instance = models.TriangleModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "base": instance.base,
            "height": instance.height,
            "area": instance.area()
        }, status=status.HTTP_200_OK)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def perimeter(self, request, pk=None):
        instance = models.TriangleModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "base": instance.base,
            "side_a": instance.side_a,
            "side_b": instance.side_b,
            "perimeter": instance.perimeter()
        }, status=status.HTTP_200_OK)


class RectangleViewSet(ShapeViewSet):
    def list(self, request):
        instance = models.RectangleModel.objects.all()
        serializer = serializers.RectangleSerializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        payload = request.data
        length = payload.get("length")
        width = payload.get("width")

        if int(length) == int(width):
            return Response({"message": "Length and Width must not be equal!"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = serializers.RectangleSerializer(data=payload)
        return super().create_or_update_shape(serializer)

    def update(self, request, pk=None):
        payload = request.data
        instance = models.RectangleModel.objects.filter(pk=pk).first()
        serializer = serializers.RectangleSerializer(instance=instance, data=payload)
        return super().create_or_update_shape(serializer)

    def retrieve(self, request, pk=None):
        instance = models.RectangleModel.objects.filter(pk=pk).first()
        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = serializers.RectangleSerializer(instance=instance, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        if models.RectangleModel.objects.filter(pk=pk).delete():
            return Response({'message': "Successfully Deleted!"}, status=status.HTTP_200_OK)

        return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def area(self, request, pk=None):
        instance = models.RectangleModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "length": instance.length,
            "width": instance.width,
            "area": instance.area()
        }, status=status.HTTP_200_OK)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def perimeter(self, request, pk=None):
        instance = models.RectangleModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "length": instance.length,
            "width": instance.width,
            "perimeter": instance.perimeter()
        }, status=status.HTTP_200_OK)


class SquareViewSet(ShapeViewSet):
    def list(self, request):
        instance = models.SquareModel.objects.all()
        serializer = serializers.SquareSerializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        payload = request.data
        serializer = serializers.SquareSerializer(data=payload)
        return super().create_or_update_shape(serializer)

    def update(self, request, pk=None):
        payload = request.data
        instance = models.SquareModel.objects.filter(pk=pk).first()
        serializer = serializers.SquareSerializer(instance=instance, data=payload)
        return super().create_or_update_shape(serializer)

    def retrieve(self, request, pk=None):
        instance = models.SquareModel.objects.filter(pk=pk).first()
        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = serializers.SquareSerializer(instance=instance, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        if models.SquareModel.objects.filter(pk=pk).delete():
            return Response({'message': "Successfully Deleted!"}, status=status.HTTP_200_OK)

        return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def area(self, request, pk=None):
        instance = models.SquareModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "side": instance.side,
            "area": instance.area()
        }, status=status.HTTP_200_OK)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def perimeter(self, request, pk=None):
        instance = models.SquareModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "side": instance.side,
            "perimeter": instance.perimeter()
        }, status=status.HTTP_200_OK)


class DiamondViewSet(ShapeViewSet):
    def list(self, request):
        instance = models.DiamondModel.objects.all()
        serializer = serializers.DiamondSerializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        payload = request.data
        serializer = serializers.DiamondSerializer(data=payload)
        return super().create_or_update_shape(serializer)

    def update(self, request, pk=None):
        payload = request.data
        instance = models.DiamondModel.objects.filter(pk=pk).first()
        serializer = serializers.DiamondSerializer(instance=instance, data=payload)
        return super().create_or_update_shape(serializer)

    def retrieve(self, request, pk=None):
        instance = models.DiamondModel.objects.filter(pk=pk).first()
        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = serializers.DiamondSerializer(instance=instance, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        if models.DiamondModel.objects.filter(pk=pk).delete():
            return Response({'message': "Successfully Deleted!"}, status=status.HTTP_200_OK)

        return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def area(self, request, pk=None):
        instance = models.DiamondModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "base": instance.base,
            "height": instance.height,
            "area": instance.area()
        }, status=status.HTTP_200_OK)

    @action(methods=["GET"], detail=True, permission_classes=[], authentication_classes=[])
    def perimeter(self, request, pk=None):
        instance = models.DiamondModel.objects.filter(pk=pk).first()

        if not instance:
            return Response({'message': "Record not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({
            "side": instance.side,
            "perimeter": instance.perimeter()
        }, status=status.HTTP_200_OK)
