from django.db import models


class TriangleModel(models.Model):
    name = models.CharField(max_length=255, unique=True)
    base = models.IntegerField()
    height = models.IntegerField()
    side_a = models.IntegerField()
    side_b = models.IntegerField()

    class Meta:
        verbose_name = 'Triangle'
        verbose_name_plural = 'Triangles'

    def area(self):
        return (self.base * self.height) / 2

    def perimeter(self):
        return self.base + self.side_a + self.side_b


class RectangleModel(models.Model):
    name = models.CharField(max_length=255, unique=True)
    length = models.IntegerField()
    width = models.IntegerField()

    class Meta:
        verbose_name = 'Rectangle'
        verbose_name_plural = 'Rectangles'

    def __str__(self):
        return "%s" % (self.name)

    def area(self):
        return self.length * self.width

    def perimeter(self):
        return (self.length + self.width) * 2


class SquareModel(models.Model):
    name = models.CharField(max_length=255, unique=True)
    side = models.IntegerField()

    class Meta:
        verbose_name = ' Square'
        verbose_name_plural = ' Squares'

    def area(self):
        return self.side**2

    def perimeter(self):
        return self.side*4


class DiamondModel(models.Model):
    name = models.CharField(max_length=255, unique=True)
    base = models.IntegerField()
    height = models.IntegerField()
    side = models.IntegerField()

    class Meta:
        verbose_name = 'Diamond'
        verbose_name_plural = 'Diamonds'

    def __str__(self):
        return "%s" % (self.name)

    def area(self):
        return (self.base + self.height) / 2

    def perimeter(self):
        return self.side*4
