from rest_framework import serializers
from django.contrib.auth.models import User
from shapeapi import models


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password']
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type': 'password'}
            }
        }

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class TriangleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TriangleModel
        fields = ['id', 'name', 'base', 'height', 'side_a', 'side_b']


class RectangleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RectangleModel
        fields = ['id', 'name', 'length', 'width']


class SquareSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SquareModel
        fields = ['id', 'name', 'side']


class DiamondSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DiamondModel
        fields = ['id', 'name', 'base', 'height', 'side']
