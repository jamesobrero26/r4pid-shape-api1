from django.apps import AppConfig


class ShapeapiConfig(AppConfig):
    name = 'shapeapi'
